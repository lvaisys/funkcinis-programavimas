module Try1
where

parse :: String -> Moves
parse ('(':'l':rest) = reverse $ parseTuples [] rest
parse _ = error "Ne sarasas"

readDigitX :: String -> (Int, String)
readDigitX ('\"':'x':'\"':'0':rest) = (0, rest)
readDigitX ('\"':'x':'\"':'1':rest) = (1, rest) 
readDigitX ('\"':'x':'\"':'2':rest) = (2, rest) 
readDigitX _ = error "Digit X expected" 
readDigitY :: String -> (Int, String)
readDigitY ('\"':'y':'\"':'0':rest) = (0, rest)
readDigitY ('\"':'y':'\"':'1':rest) = (1, rest) 
readDigitY ('\"':'y':'\"':'2':rest) = (2, rest) 
readDigitY _ = error "Digit Y expected" 

class FuzzyEq a where
  (~=) :: a -> a -> Bool
  (/~=) :: a -> a -> Bool

  a ~= b = not (a /~= b)
  a /~= b = not (a ~= b)
  
instance FuzzyEq Int where
  a ~= b = abs (a - b) <= 1

readPlayer :: String -> (Char, String)
readPlayer ('\"':'v':'\"':'\"':'x' : '\"': rest) = ('x', rest)
readPlayer ('\"':'v':'\"':'\"': 'o' : '\"': rest) = ('o', rest)
readPlayer _ = error "Player expected"

parseTuples acc ")" = acc
parseTuples acc rest =
  let
    (tuple, restt) = parseTuple rest
  in
    parseTuples (tuple:acc) restt

parseTuple :: String 
           -> ((Int, Int, Char), String) -- ^ result
parseTuple ('(':'l':rest) =
  let
    (x, restx) = readDigitX rest
    (y, resty) = readDigitY restx
    (p, restp) = readPlayer resty
  in
    case restp of
      (')':t) -> ((x, y, p), t)
      _       -> error "Tuple without closing bracket"

filt :: (a -> Bool) -> [a] -> [a]
filt p l =
  filt' [] p l
  where
    filt' :: [a] -> (a -> Bool) -> [a] -> [a]
    filt' acc _ [] = reverse acc
    filt' acc p (h:t) =
      if (p h)
        then filt' (h:acc) p t
        else filt' acc p t

type Move = (Int, Int, Char)
type Moves = [Move]

type Point = (Int, Int)
type Points = [Point]

repeated :: Points -> Bool
repeated [] = True
repeated [_] = True
repeated (h:t) = if elem h t then False else repeated t

coveredPoints :: Moves -> Points
coveredPoints [] = []
coveredPoints ((a,b,c):t)= ((a,b):coveredPoints t)


validate:: String -> Bool
validate ""= False
validate word = a&&b&&c where
  listOfMoves = (parse . filt (\a -> a/=' ')) word
  a= 10 > ( length listOfMoves)
  xMoves = length $ filt (\(_, _, a) -> a =='x') listOfMoves
  oMoves = length $ filt (\(_, _, a) -> a =='o') listOfMoves
  b = xMoves ~= oMoves
  c = repeated $ coveredPoints listOfMoves
  
  
  
  